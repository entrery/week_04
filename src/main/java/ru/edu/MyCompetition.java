package ru.edu;

import ru.edu.model.MyParticipant;
import ru.edu.model.MyAthlete;
import ru.edu.model.ScoreComparator;
import ru.edu.model.MyCountryParticipant;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.Comparator;

public class MyCompetition implements Competition {

    /**
     * Мap.
     *
     */
    private Map<Long, MyParticipant> participants = new HashMap<>();


    /**
     * Айди.
     */
    private Long id = 1L;


    /**
     * Регистрация участника.
     *
     * @param participant - участник
     * @return зарегистрированный участник
     * @throws IllegalArgumentException - при попытке повторной регистрации
     */

    public MyParticipant register(final MyAthlete participant) {
        MyParticipant newParticipant = new MyParticipant(id, 0,
                participant);
        if (participants.containsValue(newParticipant.getId())) {
            throw new IllegalArgumentException(" Спортсмен уже"
                    + " занесен в список");
        }
        participants.put(id, newParticipant);
        id++;

        return newParticipant;
    }

    /**
     * Обновление счета участника по его id.
     * Требуется константное время выполнения
     * <p>
     * updateScore(10) прибавляет 10 очков
     * updateScore(-5) отнимает 5 очков
     *
     * @param id1    регистрационный номер участника
     * @param score +/- величина изменения счета
     */
    @Override
    public void updateScore(final long id1, final long score) {
        MyParticipant participant = participants.get(id1);
        if (participant != null) {
            participant.setScore(score);
        }
    }

    /**
     * Обновление счета участника по его объекту Participant.
     * Требуется константное время выполнения
     *
     * @param participant зарегистрированный участник
     * @param score       новое значение счета
     */
    @Override
    public void updateScore(final MyParticipant participant, final long score) {
        updateScore(participant.getId(), score);
    }


    /**
     * Получение результатов.
     * Сортировка участников от большего счета к меньшему.
     *
     * @return отсортированный список участников
     */
    @Override
    public List<MyParticipant> getResults() {
        List<MyParticipant> participants1 =
                new ArrayList<>(this.participants.values());
        Comparator<MyParticipant> cmp = new ScoreComparator();
        participants1.sort(cmp);

        return participants1;
    }

    /**
     * Получение результатов по странам.
     * Группировка участников из одной страны и сумма их счетов.
     * Сортировка результатов от большего счета к меньшему.
     *
     * @return отсортированный список стран-участников
     */
    @Override
    public List<MyCountryParticipant> getParticipantsCountries() {
        Map<String, List<MyParticipant>> countriesMap = new TreeMap<>();
        Map<String, Long> countriesScore = new TreeMap<>();

        for (MyParticipant participant : participants.values()) {
            String country = participant.getAthlete().getCountry();
            if (!countriesMap.containsKey(country)) {
                countriesMap.put(country, new ArrayList<>());
            }

            countriesMap.get(country).add(participant);
            countriesScore.put(country,
                    countriesScore.getOrDefault(country, 0L)
                           + participant.getScore());
        }
        List<MyCountryParticipant> result = new ArrayList<>();
        for (Map.Entry<String, List<MyParticipant>>
                entry : countriesMap.entrySet()) {
            String country = entry.getKey();
            List<MyParticipant> participants1 = entry.getValue();
            result.add(new MyCountryParticipant(country,
                    countriesScore.get(country),
                    participants1));

        }


        return result;
    }


}
