package ru.edu;

import ru.edu.model.MyParticipant;

import ru.edu.model.MyCountryParticipant;


import java.util.List;

/**
 * Класс реализующий логику проведения соревнования.
 */
public interface Competition {


    /**
     * Обновление счета участника по его id.
     * Требуется константное время выполнения
     * <p>
     * updateScore(10) прибавляет 10 очков
     * updateScore(-5) отнимает 5 очков
     *
     * @param id    регистрационный номер участника
     * @param score +/- величина изменения счета
     */
    void updateScore(long id, long score);

    /**
     * Обновление счета участника по его объекту Participant.
     * Требуется константное время выполнения
     *
     * @param participant зарегистрированный участник
     * @param score       новое значение счета
     */


    void updateScore(MyParticipant participant, long score);

    /**
     * Получение результатов.
     * Сортировка участников от большего счета к меньшему
     *
     * @return отсортированный список участников
     */
    List<MyParticipant> getResults();

    /**
     * Получение результатов по странам.
     * Группировка участников из одной страны и сумма их счетов
     * Сортировка результатов от большего счета к меньшему
     *
     * @return отсортированный список стран-участников
     */
    List<MyCountryParticipant> getParticipantsCountries();

}
