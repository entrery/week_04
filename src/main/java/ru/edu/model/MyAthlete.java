package ru.edu.model;


import java.util.Locale;
import java.util.Objects;

public class MyAthlete implements Athlete {

    /**
     * Name.
     */
    private String firstName;

    /**
     * LastName.
     */
    private String lastName;

    /**
     * Country.
     */
    private String country;


    /**
     * Конструктор атлета.
     *
     * @param firstName1
     * @param lastName1
     * @param country1
     */
    public MyAthlete(final String firstName1, final String lastName1,
                     final String country1) {
        this.firstName = firstName1;
        this.lastName = lastName1;
        this.country = country1;
    }

    /**
     * Name.
     *
     * @return значение
     */
    @Override
    public String getFirstName() {
        return firstName;
    }

    /**
     * LastName.
     *
     * @return значение
     */
    @Override
    public String getLastName() {
        return lastName;
    }

    /**
     * Country.
     *
     * @return значение
     */
    @Override
    public String getCountry() {
        return country;
    }

    /**
     * переопределяем метод equals.
     *
     * @return значение
     */
    @Override
    public boolean equals(final Object obj) {

        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        MyAthlete that = (MyAthlete) obj;

        if (firstName != that.firstName) {
            return false;
        }
        if (lastName != that.lastName) {
            return false;
        }
        if (country != that.country) {
            return false;
        }
        return true;


    }

    /**
     * переопределяем метод hashCode.
     *
     * @return значение
     */
    @Override
    public int hashCode() {
        return Objects.hash(firstName.toLowerCase(Locale.ROOT),

                lastName.toLowerCase(Locale.ROOT),

                country.toLowerCase(Locale.ROOT));

    }

    /**
     * Строки.
     */
    @Override
    public String toString() {
        return "MyAthlete{"
                + "firstName='" + firstName + '\''
                + ", lastName='" + lastName + '\''
                + ", country='" + country + '\''
                + '}';
    }
}
