package ru.edu.model;


public class MyParticipant implements Participant {

    /**
     * Айди.
     */
    private long id;

    /**
     * Счет.
     */
    private long score;

    /**
     * Атлет.
     */
    private MyAthlete athlete;


    /**
     * Конструктор.
     *
     * @param id1
     * @param score1
     * @param athlete1
     */
    public MyParticipant(final long id1, final long score1,
                         final MyAthlete athlete1) {
        this.id = id1;
        this.score = score1;
        this.athlete = athlete1;
    }


    /**
     * Получение информации о регистрационном номере.
     *
     * @return регистрационный номер
     */
    @Override
    public Long getId() {
        return id;
    }

    /**
     * Информация о спортсмене.
     *
     * @return объект спортсмена
     */
    @Override
    public MyAthlete getAthlete() {
        return athlete;
    }

    /**
     * Счет участника.
     *
     * @return счет
     */
    @Override
    public long getScore() {

        return score;
    }

    /**
     * обновление счета участника.
     *
     * @param score1
     * @return счет
     */
    public long setScore(final long score1) {
        this.score += score1;
        return score;
    }


    /**
     * Сравнение.
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        MyParticipant other = (MyParticipant) obj;
        return athlete.equals(other.athlete);

    }

    /**
     * Хэшкод.
     */
    @Override
    public int hashCode() {
        return athlete.hashCode();
    }

    /**
     * Строки.
     */
    @Override
    public String toString() {
        return "MyParticipant{"
                + "id=" + id
                + ", score=" + score
                + ", athlete=" + athlete
                + '}';
    }


}
