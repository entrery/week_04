package ru.edu.model;

/**
 * Информация об участнике.
 */
public interface Participant {


    /**
     * Получение информации о регистрационном номере.
     *
     * @return регистрационный номер
     */
    Long getId();

    /**
     * Информация о спортсмене.
     *
     * @return объект спортсмена
     */
    Athlete getAthlete();

    /**
     * Счет участника.
     *
     * @return счет
     */
    long getScore();


}
