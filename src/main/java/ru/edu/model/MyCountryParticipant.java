package ru.edu.model;


import java.util.List;

public class MyCountryParticipant implements CountryParticipant {

    /**
     * Страна.
     */
    private String country;

    /**
     * Счет.
     */
    private long score;

    /**
     * лист.
     */
    private List<MyParticipant> listCountry;

    /**
     * конструктор.
     *
     * @param country1
     * @param score1
     * @param listCountry1
     */
    public MyCountryParticipant(final String country1, final long score1,
                                final List<MyParticipant> listCountry1) {
        this.country = country1;
        this.score = score1;
        this.listCountry = listCountry1;
    }


    /**
     * Название страны.
     *
     * @return название
     */
    @Override
    public String getName() {

        return country;
    }


    /**
     * Добавление участников.
     *
     * @param participant
     */
    public void addParticipant(final MyParticipant participant) {
        listCountry.add(participant);
        if (participant.getAthlete() == null) {
            throw new IllegalArgumentException();
        }

    }

    /**
     * Список участников от страны.
     *
     * @return список участников
     */
    @Override
    public List<MyParticipant> getParticipants() {
        return listCountry;
    }

    /**
     * Счет страны.
     *
     * @return счет
     */
    @Override
    public long getScore() {
        return score;
    }

    /**
     * Добавление счета.
     * @param score1
     */
    public void setScore(final long score1) {
        this.score += score1;
    }

    /**
     * Строки.
     */
    @Override
    public String toString() {
        return "MyCountryParticipant{"
                + "country='" + country + '\''
                + ", score=" + score
                + ", listCountry=" + listCountry
                + '}';
    }


}
