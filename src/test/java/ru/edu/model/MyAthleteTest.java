package ru.edu.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MyAthleteTest {

    MyAthlete athlete = new MyAthlete("Елизавета", "Иванова", "Россия");
    MyAthlete athlete2 = new MyAthlete("Елизавета", "Иванова", "Россия");
    MyAthlete athlete3 = new MyAthlete("Елизавета", "Трям", "Россия");

    @Test
    public void getFirstName() {
        assertEquals("Елизавета", athlete.getFirstName());
    }

    @Test
    public void getLastName() {
        assertEquals("Иванова", athlete.getLastName());
    }

    @Test
    public void getCountry() {
        assertEquals("Россия", athlete.getCountry());
    }

    @Test
    public void testEquals() {
        assertEquals(athlete, athlete2);
    }

    @Test
    public void testHashCode() {
        assertEquals(athlete.hashCode(), athlete2.hashCode());
        assertNotEquals(athlete.hashCode(), athlete3.hashCode());
    }

    @Test
    public void testToString() {
        assertNotNull(athlete.toString());
    }
}