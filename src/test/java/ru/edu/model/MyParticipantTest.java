package ru.edu.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class MyParticipantTest {

    MyAthlete athlete1 = new MyAthlete("Володя", "Лысый", "Россия");
    MyAthlete athlete2 = new MyAthlete("Шарик", "Пес", "Россия");
    MyAthlete athlete3 = new MyAthlete("Шарик", "НеПес", "Россия");

    MyParticipant participant1 = new MyParticipant(100, 10, athlete1);
    MyParticipant participant2 = new MyParticipant(101, 10, athlete2);
    MyParticipant participant3 = new MyParticipant(101, 10, athlete2);

    @Test
    public void getId() {
        assertEquals(100, (long) participant1.getId());
    }

    @Test
    public void getAthlete() {
        assertEquals(athlete1, participant1.getAthlete());
    }

    @Test
    public void getScore() {
        assertEquals(10, (long) participant1.getScore());
    }

    @Test
    public void setScore() {
        participant1.setScore(10);
        assertEquals(20, participant1.getScore());
    }

    @Test
    public void testEquals() {
        assertEquals(participant2, participant3);
        assertNotEquals(participant1, participant2);
    }

    @Test
    public void testHashCode() {
        assertEquals(participant2, participant3);
        assertNotEquals(participant1, participant2);
    }

    @Test
    public void testToString() {
        assertNotNull(participant1.toString());
    }
}