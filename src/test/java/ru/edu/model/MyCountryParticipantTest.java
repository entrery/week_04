package ru.edu.model;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class MyCountryParticipantTest {


    MyAthlete athlete = new MyAthlete("Вова", "Пупкин", "Россия");
    MyParticipant participant = new MyParticipant(10, 1, athlete);
    List<MyParticipant> participantList = new ArrayList<>();

    MyCountryParticipant country = new MyCountryParticipant("Россия", 10, participantList);


    @Test
    public void getName() {
        participantList.add(participant);
        assertEquals("Россия", country.getName());
    }

    @Test
    public void addParticipant() {
        participantList.add(participant);
        assertNotNull(country.getParticipants());
    }

    @Test
    public void getParticipants() {
        participantList.add(participant);
        assertNotNull(country.getParticipants());

    }

    @Test
    public void getScore() {
        country.setScore(100);
        assertEquals(110, country.getScore());
    }

    @Test
    public void setScore() {
        country.setScore(50);
        assertEquals(60, country.getScore());
    }

    @Test
    public void testToString() {
        assertNotNull(country.toString());
    }
}