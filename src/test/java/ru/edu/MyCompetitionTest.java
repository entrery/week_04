package ru.edu;

import org.junit.Test;
import ru.edu.model.MyAthlete;
import ru.edu.model.MyCountryParticipant;
import ru.edu.model.MyParticipant;
import ru.edu.model.ScoreComparator;


import java.util.Comparator;
import java.util.List;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

public class MyCompetitionTest {

    MyAthlete athlete1 = new MyAthlete("Борис", "Джонсон", "Англия");
    MyAthlete athlete2 = new MyAthlete("Борист", "Джонстон", "Англия");
    MyAthlete athlete3 = new MyAthlete("Санта", "Клаус", "Штаты");

    MyCompetition competition = new MyCompetition();


    @Test
    public void register() {
        competition.register(athlete1);
        assertEquals(1, competition.getResults().size());
    }

    @Test
    public void updateScore() {
        MyCompetition competition = new MyCompetition();
        MyParticipant participant1 = competition.register(athlete2);
        competition.updateScore(participant1.getId(), 10);
        assertEquals(10, (long) participant1.getScore());
    }

    @Test
    public void testUpdateScore() {
        MyCompetition competition = new MyCompetition();
        MyParticipant participant1 = competition.register(athlete2);
        competition.updateScore(participant1, 10);
        competition.updateScore(participant1, 10);
        assertEquals(20, (long) participant1.getScore());
    }

    @Test
    public void getResults() {
        MyCompetition competition = new MyCompetition();
        MyParticipant participant1 = competition.register(athlete1);
        MyParticipant participant2 = competition.register(athlete2);
        MyParticipant participant3 = competition.register(athlete3);
        competition.updateScore(participant1, 10);
        competition.updateScore(participant3, 30);
        competition.updateScore(participant2, 20);

        List<MyParticipant> participants = competition.getResults();
        assertEquals("[MyParticipant{id=3, score=30, athlete=MyAthlete{firstName='Санта', lastName='Клаус', " +
                "country='Штаты'}}, MyParticipant{id=2, score=20, athlete=MyAthlete{firstName='Борист', " +
                "lastName='Джонстон', country='Англия'}}, MyParticipant{id=1, score=10, " +
                "athlete=MyAthlete{firstName='Борис', lastName='Джонсон', " +
                "country='Англия'}}]", participants.toString());


    }

    @Test
    public void getParticipantsCountries() {
        MyCompetition competition = new MyCompetition();
        MyParticipant participant1 = competition.register(athlete1);
        MyParticipant participant2 = competition.register(athlete2);
        MyParticipant participant3 = competition.register(athlete3);
        competition.updateScore(participant1, 10);
        competition.updateScore(participant3, 30);
        competition.updateScore(participant2, 20);

        List<MyCountryParticipant> list = competition.getParticipantsCountries();
        assertEquals("Англия", list.get(0).getName());
        assertEquals("Штаты", list.get(1).getName());


    }
}